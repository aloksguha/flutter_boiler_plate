import 'package:flutter/material.dart';
import './utils/routes.dart';
import './theme/themes.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Boiler Plate Demo',
      routes: Routes.routes,
      theme: mainTheme,
      home: MyHomePage(title: 'Boiler Plate Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RaisedButton(
                  child: Text('Login'),
                  onPressed: () {
                    Navigator.of(context).pushNamed(Routes.login);
                  }),
              RaisedButton(
                  child: Text('Register'),
                  onPressed: () {
                    Navigator.of(context).pushNamed(Routes.register);
                  })
            ],
          ),
        ));
  }
}
