import 'package:boilerplate/theme/themes.dart';

import '../utils/routes.dart';
import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: secondaryTheme,
      routes: Routes.routes,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Register'),
        ),
        body: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Rrgister Page Example',
              style: TextStyle(fontSize: 24),
            ),
            RaisedButton(
                child: Text('Login'),
                onPressed: () {
                  Navigator.of(context).pushNamed(Routes.login);
                })
          ],
        )),
      ),
    );
  }
}
